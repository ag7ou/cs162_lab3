// Lab 3 - Rob Johnson - PCC cs162 - 10/6/18

#include <iostream>
#include <fstream>

#define MAX_NAME_SIZE (250)
#define MAX_NUM_ENTRIES (100)
#define INPUT_FILENAME ("employees.txt")
#define OUTPUT_FILENAME ("grosspay.txt")

using namespace std;

struct employee
{
    char name[MAX_NAME_SIZE];
    int id;
    double hours;
    double payRate;
    double grossPay;
};

// returns the number of entries that have been read
int fileRead(struct employee *employees)
{
    int len = 0;
    ifstream infile;

    infile.open(INPUT_FILENAME);
    if(infile)
    {
        infile.peek(); // tests for end of file w/o moving the read pointer ahead
        for(int i = 0; !infile.eof(); ++i)
        {
            infile.getline(employees[i].name, MAX_NAME_SIZE);

            infile >> employees[i].id;
            infile.ignore(MAX_NAME_SIZE,'\n');

            infile >> employees[i].hours;
            infile.ignore(MAX_NAME_SIZE,'\n');

            infile >> employees[i].payRate;
            infile.ignore(MAX_NAME_SIZE,'\n');

            ++len;

            infile.peek();
        }
        infile.close();
    }
    else
    {
        cout << "Error opening input file." << endl;
    }

    return len;
}


void fileWrite(struct employee *employees, int len)
{
    ofstream outfile;

    outfile.open(OUTPUT_FILENAME);
    outfile.precision(2);
    outfile << fixed;

    for(int i = 0; i < len; ++i)
    {
        outfile << employees[i].id << ' ' << employees[i].grossPay << endl;
    }

    outfile.close();
}

void printEmployee(struct employee individual)
{
    cout << "======== Employee ============" << endl;
    cout << "name: " << individual.name << endl;
    cout << "ID: " << individual.id << endl;
    cout << "hours: " << individual.hours << endl;
    cout << "payrate: " << individual.payRate << endl;
}

void printGross(struct employee individual)
{
    cout << individual.name << " gross pay is " << individual.grossPay << endl;
}

int main()
{
    struct employee employees[MAX_NUM_ENTRIES];
    int numEmployees = 0;

    cout << "--------------------------------\n" << endl;

    cout.precision(2);
    cout << fixed;

    numEmployees = fileRead(employees);

    for(int i = 0; i < numEmployees; ++i)
    {
        printEmployee(employees[i]);
    }

    cout << "\nCalculating gross pay ..." << endl;
    for(int i = 0; i < numEmployees; ++i)
    {
        employees[i].grossPay = employees[i].hours * employees[i].payRate;
        printGross(employees[i]);
    }

    fileWrite(employees, numEmployees);

    cout << "\ngross pay has been written to " << OUTPUT_FILENAME << endl;

    cout << "\n===============================" << endl;
    return 0;
}
